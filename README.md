# Express.js Backend Project


# API's List

1. Users
**http://localhost:`5000/api/users**`

![](https://i.ibb.co/FVVMv2r/Screenshot-1.png)



2. Pets:
**http://localhost:`5000/api/pets**`

**GET**
![](https://i.ibb.co/0r6tys9/Screenshot-2.png)

**POST**
![](https://i.ibb.co/60vwNZJ/Screenshot-4.png)


3. Owners:
**http://localhost:`5000/api/owners**`

![](https://i.ibb.co/9VxkN4G/Screenshot-3.png)







