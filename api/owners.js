const express = require('express');
const router = express.Router();
const owners = require('../res/Owners');


//Get all Pets
router.get('/', (req, res) => res.json(owners));

module.exports = router