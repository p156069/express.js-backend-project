const express = require('express');
const router = express.Router();
const pets = require('../res/Pets');


//Get all Pets
router.get('/', (req, res) => res.json(pets));


//Add pets
router.post('/', (req, res) => {
    const newPet = {
        name : req.body.name,
        species : req.body.species,
      };

      if(!newPet.name || !newPet.species){
        return res.status(400).json({msg: 'Please include a name and specie'});
      }

      pets.push(newPet);
      res.json(pets);
});


//Update pets

router.put('/:name', (req, res) =>{
  const found = pets.some(pet => pet.name === req.params.name);

  if(found){
    const updatePet = req.body;
    pets.forEach(pet =>{
      if(pet.name === pet.params.name){
        pet.name = updatePet.name ? updatePet.name: pet.name;
        pet.species = updatePet.species ? updatePet.species: pet.species;
        
        res.json({msg:"Updated"});
      }
    });
  }
  else{
    res.status(400).json({msg: 'No Pet Found'});
  }

});

module.exports = router