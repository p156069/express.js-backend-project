const Pets = [
      {
        name : "Purrsloud",
        species : "Cat",
      },
      {
        name : "Barksalot",
        species : "Dog",
      },
      {
        name : "Meowsalot",
        species : "Cat",
      }
    ]

module.exports = Pets;
