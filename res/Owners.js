const Owners = [
    {
		name: 'Mansoor',
		last: 'Ahmad',
		pets:[
			{
			 	name:'Purrsloud',
			 	species: 'Cat'
			}
        ]
    },
    {
		name: 'Fawad',
		last: 'Ahmad',
		pets:[
			{
                name:'Barksalot',
                species: 'Dog'
			}
        ]
    },
    {
		name: 'Maad',
		last: 'Ahmad',
		pets:[
			{
                name:'Meowsalot',
                species: 'Cat'
            }

        ]
    }
]

module.exports = Owners;