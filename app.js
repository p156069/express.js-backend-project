const express = require('express');
const app = express();

//Body Parser Middleware
app.use(express.json());
app.use(express.urlencoded({extended: false}));
//Routes
app.use('/api/pets', require('./api/pets'));
app.use('/api/users', require('./api/users'));
app.use('/api/owners', require('./api/owners'));

const PORT = process.env.PORT || 5000;
app.listen(PORT, ()=> console.log(`Server started on ${PORT}`));